import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs/index";
import { ApiResponse, Job } from "../_models";
import { environment } from '../../environments/environment';

@Injectable()
export class JobService {

  constructor(private http: HttpClient) { }

  getJobs(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${environment.apiUrl}/job`);
  }

  getJobById(id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(`${environment.apiUrl}/job/` + id);
  }

  createJob(job: Job): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}/scheduleJob`, job);
  }

  updateJob(job: Job): Observable<ApiResponse> {
    return this.http.put<ApiResponse>(`${environment.apiUrl}/job/` + job.id, job);
  }

  deleteJob(id: number): Observable<ApiResponse> {
    return this.http.delete<ApiResponse>(`${environment.apiUrl}/job/` + id);
  }
}
