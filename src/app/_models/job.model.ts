export class Job {
    id: number;
    wellNames: string;
    endDepth: number;
    startDepth: number;
    startTimeLog: string;
    endTimeLog: string;
    frequency: string
    _day: string;
    _date: number;
    _hour: number;
    _minute: number;
    status: string;
}
