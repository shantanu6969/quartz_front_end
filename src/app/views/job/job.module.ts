// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CreateJobComponent } from './create-job.component';
import { JobListComponent } from './job-list.component';

// Job Routing
import { JobRoutingModule } from './job-routing.module';
import { DataTablesModule } from 'angular-datatables';
import { JobService } from '../../_services';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    JobRoutingModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    NgxDaterangepickerMd,
    NgMultiSelectDropDownModule.forRoot()
  ],
  declarations: [
    CreateJobComponent,
    JobListComponent
  ],
  providers: [ JobService ]
})
export class JobModule { }
