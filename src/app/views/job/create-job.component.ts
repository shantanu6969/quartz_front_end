import { Component, OnInit } from '@angular/core';
import { Job } from '../../_models';
import { JobService } from '../../_services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { ToastrManager } from 'ng6-toastr-notifications';
import { Router } from '@angular/router';


@Component({
  selector: 'app-create-job',
  templateUrl: './create-job.component.html',
  styleUrls: ['./create-job.component.scss']
})
export class CreateJobComponent implements OnInit {

  job = new Job();
  jobForm: FormGroup;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  frequency = 'daily';
  dates = [];
  hours = [];
  minutes = [];
  submitted = false;

  constructor(
    private toastr: ToastrManager,
    private router: Router,
    private formBuilder: FormBuilder,
    private jobService: JobService
  ) { }

  buildForm() {
    this.jobForm = this.formBuilder.group({
      wellNames: ['', Validators.required],
      startDepth: ['', Validators.required],
      endDepth: ['', Validators.required],
      startEndTimeLog: ['', Validators.required],
      _date: [''],
      _day: [''],
      _hour: ['', Validators.required],
      _minute: ['', Validators.required],
      frequency: ['daily']
    });
  }

  setFrequencyValidators() {
    const _dayControl = this.jobForm.get('_day');
    const _dateControl = this.jobForm.get('_date');

    this.jobForm.get('frequency').valueChanges
      .subscribe(frequency => {

        if (frequency === 'daily') {
          _dayControl.setValidators(null);
          _dateControl.setValidators(null);
        }

        if (frequency === 'weekly') {
          _dayControl.setValidators([Validators.required]);
          _dateControl.setValidators(null);
        }

        if (frequency === 'monthly') {
          _dayControl.setValidators(null);
          _dateControl.setValidators([Validators.required]);
        }

        _dayControl.updateValueAndValidity();
        _dateControl.updateValueAndValidity();
      });
  }

  ngOnInit() {
    this.buildForm();
    this.setFrequencyValidators();
    this.dates = Array(31).fill(0).map((x, i) => i + 1);
    this.hours = Array(24).fill(0).map((x, i) => i + 1);
    this.minutes = Array(60).fill(0).map((x, i) => i + 1);
    this.dropdownList = [
      { item_id: 1, item_text: 'Mumbai' },
      { item_id: 2, item_text: 'Bangaluru' },
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' },
      { item_id: 5, item_text: 'New Delhi' }
    ];
    this.selectedItems = [
      { item_id: 3, item_text: 'Pune' },
      { item_id: 4, item_text: 'Navsari' }
    ];
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
  }

  isInvalidDate(date) {
    return date.weekday() === 0;
  }

  // convenience getter for easy access to form fields
  get f() { return this.jobForm.controls; }


  onSubmit(): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.jobForm.invalid) {
      return;
    }

    this.job.wellNames = Array.prototype.map.call(this.jobForm.controls.wellNames.value, s => s.item_text).toString();
    this.job.startDepth = this.jobForm.controls.startDepth.value;
    this.job.endDepth = this.jobForm.controls.endDepth.value;

    let startEndTimeLog = this.jobForm.controls.startEndTimeLog.value;
    this.job.startTimeLog = moment(startEndTimeLog.startDate).local().format('YYYY-MM-DDTHH:mm:ss');
    this.job.endTimeLog = moment(startEndTimeLog.endDate).local().format('YYYY-MM-DDTHH:mm:ss');

    this.job.frequency = this.jobForm.controls.frequency.value;
    this.job._day = this.jobForm.controls._day.value;
    this.job._date = this.jobForm.controls._date.value;
    this.job._hour = this.jobForm.controls._hour.value;
    this.job._minute = this.jobForm.controls._minute.value;


    this.jobService.createJob(this.job).subscribe(data => {
      if (data.success) {
        this.toastr.successToastr('Job created successfully.', 'Success!');
        this.router.navigate(['/job/job-list']);
      }
    });

  }

  setFrequency(frequency: string): void {
    this.frequency = frequency;
  }
}
