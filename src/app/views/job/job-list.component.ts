import { Component, OnInit, ViewChild } from '@angular/core';
import { JobService } from '../../_services';
import { Job } from '../../_models';
import { Subject } from 'rxjs';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.scss']
})
export class JobListComponent implements OnInit {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  jobs: Job[];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  constructor(private jobService: JobService) { }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      ordering: false,
    };
     this.getJobs();
  }

  getJobs() {
    return this.jobService.getJobs()
      .subscribe(
        data => {
          this.jobs = data.result;
        }
      );
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  changeJobStatus(job: Job, status: string): void {
    job.status = status;
    this.jobService.updateJob(job)
      .subscribe(
        data => {
          this.rerender();
        }
      );
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first 
      dtInstance.destroy();
      // Call the dtTrigger to rerender again // 
      this.dtTrigger.next();
    });
  }

}
