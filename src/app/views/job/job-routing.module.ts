import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { JobListComponent } from './job-list.component';
import { CreateJobComponent } from './create-job.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Job'
    },
    children: [
      {
        path: '',
        redirectTo: 'job-list'
      },
      {
        path: 'create-job',
        component: CreateJobComponent,
        data: {
          title: 'Create Job'
        }
      },
      {
        path: 'job-list',
        component: JobListComponent,
        data: {
          title: 'Job List'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobRoutingModule {}
